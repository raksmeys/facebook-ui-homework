/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import AvatarDemo from './src/components/AvatarDemo';
import BadgeDemo from './src/components/BadgeDemo';

AppRegistry.registerComponent(appName, () => App);
