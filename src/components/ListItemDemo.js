import React, { Component } from 'react'
import { View, Text, ActivityIndicator } from 'react-native'
import { Card, ListItem, Button, Icon, CheckBox, Divider, Header, Image, Input, Avatar } from 'react-native-elements'

const users = [
    {
        name: 'Dara',
        avatar: 'https://pngimg.com/uploads/github/github_PNG53.png',
        subtitle: 'Hey'
    },
    {
        name: 'Dara',
        avatar: 'https://pngimg.com/uploads/github/github_PNG53.png',
        subtitle: 'Hey'
    },
    {
        name: 'Dara',
        avatar: 'https://pngimg.com/uploads/github/github_PNG53.png',
        subtitle: 'Hey'
    },
    {
        name: 'Dara',
        avatar: 'https://pngimg.com/uploads/github/github_PNG53.png',
        subtitle: 'Hey'
    }


]

export class ListItemDemo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            checked: false,
            username: '',
            password: ''
        }
    }
    getImage = () => {
        alert('hi')
    }
    render() {
        return (
            <View>
                <Header
                    leftComponent={{ icon: 'menu', color: '#fff', onPress: () => this.getImage() }}
                    centerComponent={{ text: 'Home', style: { color: '#fff' } }}
                    rightComponent={{ icon: 'alarm-add', color: '#fff' }}
                />
                <Input
                    placeholder='Username'
                    leftIcon={{ type: 'material', name: 'people', color: 'gray' }}
                    onChangeText={(username) => this.setState({
                        username: username
                    })}
                />
                <Input
                    placeholder='Password'
                    secureTextEntry={true}
                    leftIcon={{ type: 'material', name: 'lock', color: 'gray' }}
                />



                <View>
                    {
                        users.map((l, i) => (
                            <ListItem key={i} bottomDivider>
                                <Avatar source={{ uri: l.avatar }} />
                                <ListItem.Content>
                                    <ListItem.Title>{l.name}</ListItem.Title>
                                    <ListItem.Subtitle>{l.subtitle}</ListItem.Subtitle>
                                </ListItem.Content>
                            </ListItem>
                        ))
                    }
                </View>


                <Button
                    type="outline"
                    icon={
                        <Icon
                            type="material"
                            name="facebook"
                            size={15}
                            color="white"
                        />
                    }
                    title={this.state.username}
                />
                <Divider />
                <Image
                    source={{ uri: 'https://pngimg.com/uploads/github/github_PNG53.png' }}
                    style={{ width: 200, height: 200 }}
                    PlaceholderContent={<ActivityIndicator />}
                />
                <Icon
                    name="android"
                    type="material"
                    color="green"
                />

                <Icon
                    name='sc-telegram'
                    type='evilicon'
                    color='#517fa4'
                    size={50}
                />
                <CheckBox
                    center
                    title='Click Here'
                    checked={this.state.checked}
                    onPress={() => this.setState({ checked: !this.state.checked })}

                />
                <Divider style={{ backgroundColor: 'blue' }} />
                <CheckBox
                    center
                    title='Click Here'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checked}
                    onPress={() => this.setState({ checked: !this.state.checked })}
                />

                <CheckBox
                    center
                    title='Click Here'
                    checked={this.state.checked}
                />

                <CheckBox
                    center
                    title='Click Here'
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checked}
                />

                <CheckBox
                    center
                    title='Click Here to Remove This Item'
                    iconRight
                    iconType='material'
                    checkedIcon='clear'
                    uncheckedIcon='add'
                    checkedColor='red'
                    checked={this.state.checked}
                />

                {/* <CheckBox
                    checkedIcon={<Image source={require('../checked.png')} />}
                    uncheckedIcon={<Image source={require('../unchecked.png')} />}
                    checked={this.state.checked}
                    onPress={() => this.setState({ checked: !this.state.checked })}
                /> */}

                {/* <Text>implemented without image without header, using ListItem component</Text> */}
                <Card containerStyle={{ padding: 0 }} >
                    {
                        users.map((u, i) => {
                            return (
                                <ListItem
                                    key={i}
                                    roundAvatar
                                    title={u.name}
                                    leftAvatar={{ source: { uri: u.avatar } }}
                                />
                            );
                        })
                    }
                </Card>


                {/* <Text>implemented with Text and Button as children</Text>
                <Card>
                    <Card.Title>HELLO WORLD</Card.Title>
                    <Card.Divider />
                    <Card.Image source={require('./../img/github.png')} />
                    <Text style={{ marginBottom: 10 }}>
                        The idea with React Native Elements is more about component structure than actual design.
                    </Text>
                    <Button
                        icon={<Icon name='code' color='#ffffff' />}
                        buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
                        title='VIEW NOW' />
                </Card> */}
            </View>
        )
    }
}

export default ListItemDemo
