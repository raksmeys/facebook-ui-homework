import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { Badge, Avatar, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';

export class BadgeDemo extends Component {
    render() {
        return (
            <View>
                <Avatar
                    rounded
                    source={{
                        uri: 'https://randomuser.me/api/portraits/men/41.jpg',
                    }}
                    size="large"
                />

                <Badge
                    value="99+" status="error"
                    containerStyle={{ position: 'absolute', top: -4, right: -4 }}
                />




                <Button
                    title="Solid Button"
                />

                <Button
                    title="Outline button"
                    type="outline"
                />

                <Button
                    title="Clear button"
                    type="clear"
                />

                <Button
                    icon={
                        <Icon
                            name="arrow-right"
                            size={15}
                            color="white"
                        />
                    }
                    title="Button with icon component"
                />

                <Button
                    icon={{
                        name: "arrow-right",
                        size: 15,
                        color: "white"
                    }}
                    title="Button with icon object"
                />

                <Button
                    icon={
                        <Icon
                            name="arrow-right"
                            size={15}
                            color="white"
                        />
                    }
                    iconRight
                    title="Button with right icon"
                />

                <Button
                    title="Loading button"
                    loading
                />
            </View>
        )
    }
}

export default BadgeDemo

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue'
    }
})
