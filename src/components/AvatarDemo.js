import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import { Avatar, Accessory} from 'react-native-elements';

export class AvatarDemo extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image 
                    source={require('../img/github.png')}
                    style={{width: 36, height: 36}}
                />
                <Avatar
                    rounded
                    size="large"
                    source={require('../img/github.png')}
                    style={{width: 36, height: 36}}
                />
                <Avatar rounded title="MD" />
                <Avatar rounded icon={{ name: 'home', color: 'black' }} />
                <Avatar
                    source={require('../img/github.png')}
                    style={{width: 36, height: 36}}
                >
                    <Accessory />
                </Avatar>
            </View>
        )
    }
}

export default AvatarDemo

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue'
    }
})
