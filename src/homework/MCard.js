import React, { Component } from 'react'
import { Card, ListItem, Button, Icon, Avatar } from 'react-native-elements'
import { View, Text, Image, StyleSheet, ScrollView, FlatList } from 'react-native'
const DATA = [
    {
        name: "Chan Chhaya",
        avatar: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/118861827_380432846455348_76032938965531068_o.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGSbkeYhVXlrmHYsYHg1a_-M1u2coKISYczW7ZygohJh-gSn2kz50O8AiEIFOZM8w7pZqb9EYebbx8di0Kh9xLd&_nc_ohc=ICtUjz6jofcAX-6ky8o&_nc_ht=scontent.fpnh7-1.fna&oh=190e1e8e062359507be4e96041cfc407&oe=5FD9E1A2',
        storyImage: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/118861827_380432846455348_76032938965531068_o.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGSbkeYhVXlrmHYsYHg1a_-M1u2coKISYczW7ZygohJh-gSn2kz50O8AiEIFOZM8w7pZqb9EYebbx8di0Kh9xLd&_nc_ohc=ICtUjz6jofcAX-6ky8o&_nc_ht=scontent.fpnh7-1.fna&oh=190e1e8e062359507be4e96041cfc407&oe=5FD9E1A2'
    },
    {
        name: 'Mii Roth',
        avatar: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/118861827_380432846455348_76032938965531068_o.jpg?_nc_cat=108&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGSbkeYhVXlrmHYsYHg1a_-M1u2coKISYczW7ZygohJh-gSn2kz50O8AiEIFOZM8w7pZqb9EYebbx8di0Kh9xLd&_nc_ohc=ICtUjz6jofcAX-6ky8o&_nc_ht=scontent.fpnh7-1.fna&oh=190e1e8e062359507be4e96041cfc407&oe=5FD9E1A2',
        storyImage: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/125419764_396198401508066_6858037386935585062_o.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeGYeIA36tyvEs28tjPqqRMg8OpH8n5AL-vw6kfyfkAv690rjVIkjYAw-gwxBfS1naNZZyres9HS4BB_wG49EaT_&_nc_ohc=zpjwAaLJZgEAX-M10ni&_nc_ht=scontent.fpnh7-1.fna&oh=3a2b78445432fb22138da758a3671488&oe=5FDA300A'
    },
    {
        name: 'Liza',
        avatar: 'https://scontent.fpnh4-1.fna.fbcdn.net/v/t1.0-1/p320x320/72308756_108983713850684_4885224595654705152_o.jpg?_nc_cat=101&_nc_oc=AQkuVwwjAOOq0P4Y_G4VjBN-CRkFIVTPZ7ZLH1cmYwPfD8hItcbpOTl-RzqM5b7tCIU&_nc_ht=scontent.fpnh4-1.fna&oh=a6b35bec3f19bd9feb793a4b6955e081&oe=5E49245D',
        storyImage: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/125251956_953259445162979_4303426547078984561_n.jpg?_nc_cat=111&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFj7tMezzX-FSh2AP-rDWEKhCZ8oEJ-N2qEJnygQn43aroRBdSMz40tsJJaLagcs895iLJR1zCkIEi_iey0lq8P&_nc_ohc=wENnkcvm-4sAX9jT3at&_nc_ht=scontent.fpnh7-1.fna&oh=974ec7a2d721627f61e5ae106d047c90&oe=5FDC3027'
    },
    {
        name: 'Teav Tuy',
        avatar: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/123849688_388515892276317_4850340387651728562_o.jpg?_nc_cat=103&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeGWBv56BMIDQhe40prXe32SMj4YxVSB6M0yPhjFVIHozUM_jECsybPEWYXWWS1svhnIAARHUONtDpweiQSEpSfL&_nc_ohc=l8yrwmAQZMMAX81av94&_nc_ht=scontent.fpnh7-1.fna&oh=78c139f72ceeffe16eef141a399d698f&oe=5FDBBB34',
        storyImage: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/122594581_378582116603028_4695853208483107811_o.jpg?_nc_cat=104&ccb=2&_nc_sid=a4a2d7&_nc_eui2=AeFswF175Ds907qkow-ziirgYXOSASdNc1xhc5IBJ01zXPQOxmsYVcfsLet9fj08Dj_MsUvxVWr9Wg_4JakCXb3M&_nc_ohc=EZ8P9TfaKc8AX85FI1M&_nc_ht=scontent.fpnh7-1.fna&oh=189aab0e0a624e8a18ce30e7970ee332&oe=5FDCB83F'
    },
    {
        name: 'Polen Sok',
        avatar: 'https://scontent.fpnh4-1.fna.fbcdn.net/v/t1.0-1/c1.0.320.320a/p320x320/60294109_1344137332416483_2353882759585333248_n.jpg?_nc_cat=104&_nc_oc=AQmZMtk8tuMnl5hqd_WxebPpN6YFvfTinjNtboe6PNCNyEXD_SBtvWELB3tuaDfVQ7A&_nc_ht=scontent.fpnh4-1.fna&oh=999baad32251d2cdeec7eb1a28e85d01&oe=5E5346E8',
        storyImage: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/125251956_953259445162979_4303426547078984561_n.jpg?_nc_cat=111&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFj7tMezzX-FSh2AP-rDWEKhCZ8oEJ-N2qEJnygQn43aroRBdSMz40tsJJaLagcs895iLJR1zCkIEi_iey0lq8P&_nc_ohc=wENnkcvm-4sAX9jT3at&_nc_ht=scontent.fpnh7-1.fna&oh=974ec7a2d721627f61e5ae106d047c90&oe=5FDC3027'
    },
    {
        name: 'Raen Real',
        avatar: 'https://scontent.fpnh4-1.fna.fbcdn.net/v/t1.0-1/p320x320/71849970_136754964362636_1791479284917862400_n.jpg?_nc_cat=102&_nc_oc=AQktjD_DuyydZLkJpkA6ZC3xRFtoj7xfDG50eImqtpOnNpx-8F97mcBaI0v9cgNLM38&_nc_ht=scontent.fpnh4-1.fna&oh=519135cab947cd93bf95aa5447082639&oe=5E46A902',
        storyImage: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/125360138_1323721867983004_3496319316110804587_n.jpg?_nc_cat=111&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeHiXfzoz5VmOWAG58Eiha1HvK5Wf-lTQTW8rlZ_6VNBNSMxaLuIgJ6KssyVRTOYEZ48cwswV7d0_UURZFu2NnnJ&_nc_ohc=4F-ZCr36y2wAX86jGp6&_nc_ht=scontent.fpnh7-1.fna&oh=9e6693a5e7971c866f327d594fba01c6&oe=5FDAA2A9'
    },
    {
        name: 'Kong ',
        avatar: 'https://scontent.fpnh4-1.fna.fbcdn.net/v/t1.0-1/c106.0.320.320a/p320x320/74393788_2463208770667604_9050522907683323904_o.jpg?_nc_cat=105&_nc_oc=AQmQ1vKEHAHYlf3YTiFC9tgbmJ8bz3EE2SnY4XeXWKE0Ipa6IotXjromDgqdnmEF6co&_nc_ht=scontent.fpnh4-1.fna&oh=b120075dff95325e8630b529d465c259&oe=5E894312',
        storyImage: 'https://mdbootstrap.com/img/Photos/Others/images/43.jpg'
    },
   
]
function Item({ name, avatar, storyImage }) {
    return (
        <View style={styles.containerCard}>
            <View style={styles.card}>
            <Avatar
                rounded
                source={{uri: avatar}}
            />
            <Text style={{color: 'white', marginTop: 100, fontWeight: 'bold',}}>{name}</Text>
        </View>
        <Image

        resizeMode="cover"
        style={{width: 130, height: 170, borderRadius: 5}}
        source={{uri: storyImage}}
        />
        </View>
    );
  }
export default class MCard extends Component {

    render() {
        return (
            <View style={styles.container}>
            <ScrollView horizontal={true} horizontalIndicator={false}>               
                <View  style={styles.mainCard}>
                    <View style={styles.containerCard}>
                        <View style={styles.card}>
                            <Avatar
                                rounded
                                icon={{name: 'star', type: 'material'}}
                                
                            />
    
                            <Text style={{color: 'white', marginTop: 100, fontWeight: 'bold',}}>Add To Story</Text>
                        </View>
                        <Image
                        resizeMode="cover"
                        style={{width: 130, height: 170, borderRadius: 5,}}
                        source={require('./../img/reksmey.jpg')}
                        />
                    </View>
                    {/* loop */}
                    <View style={styles.containerCard}>
                        <View style={styles.card}>
                            <Avatar
                                rounded
                                source={{uri: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/90215258_145899293623164_542864470142091264_n.jpg?_nc_cat=104&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFdEMyRjdZ31l2NeX_zQu0azd5ZpQKbKq_N3lmlApsqr0linwisvybBrcBHFcbGlsau2965c_rqU3WgnbCIGIV_&_nc_ohc=FR5SqBn09o8AX84EKsd&_nc_ht=scontent.fpnh7-1.fna&oh=f971426ceb3d0fe68f77e8dd6aeedee2&oe=5FDA1389'}}
                            />
                            <Text style={{color: 'white', marginTop: 100, fontWeight: 'bold',}}>Reksmey</Text>
                        </View>
                        <Image
                        resizeMode="cover"
                        style={{width: 130, height: 170, borderRadius: 5,}}
                        source={require('./../img/reksmey.jpg')}
                        />
                    </View>

                    {/* loop */}

                         <FlatList
                                horizontal
                                pagingEnabled={true}
                                horizontalIndicator={false}
                                legacyImplementation={false}
                                data={DATA}
                                renderItem={({ item }) => <Item name={item.name} storyImage={item.storyImage} avatar={item.avatar} />}
                                keyExtractor={item => item.id}
                            />
                           
                </View>
                        
            </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    mainCard:{
        flexDirection: 'row',
        margin: 10,
    },
    containerCard:{
        marginLeft: 7
    },
    card:{
        position: 'absolute',
        zIndex: 1,
        paddingTop: 10,
        paddingLeft: 10
    }
})
