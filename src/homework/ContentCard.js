import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, FlatList, ScrollView, TouchableOpacity } from 'react-native'
import { Card, Avatar, Icon, Divider } from 'react-native-elements'
import DATA from './data/DATA'
const { width: WIDTH } = Dimensions.get('screen')

function Content({ name, avatar, hour, captions, image }) {
    return (
        <View>
            <View style={styles.more}>
                <View style={styles.content}>
                    <Avatar
                        rounded
                        source={{
                            uri: avatar,
                        }}
                    />
                    <View style={styles.profile}>
                        <Text style={{ fontWeight: 'bold', }}> {name} </Text>
                        <Text style={{ color: 'grey', }}>{hour}</Text>
                    </View>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        this.Scrollable.open();
                    }}
                >
                    <Image
                        source={require('./../img/more.png')}
                        style={{ width: 30, height: 30 }}

                    />
                </TouchableOpacity>
            </View>
            <View style={styles.contentData}>
                <Text
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={styles.captions}>{captions}</Text>
                <Image
                    resizeMode='cover'
                    style={{ width: WIDTH, height: WIDTH - 150, marginBottom: 10 }}
                    source={{ uri: image }}
                />

                <View
                    style={{flex: 1, justifyContent: 'space-between', flexDirection: 'row'}}
                >
                    <Text
                        style={{ marginLeft: 10, marginBottom: 10 }}
                    >Messager</Text>

                    <Image
                        source={{ uri: 'https://icon-library.com/images/bookmark-icon/bookmark-icon-5.jpg' }}
                        style={{ width: 36, height: 36, tintColor: 'gray', marginTop: -10 }}
                    />
                </View>


                <View
                    style={{flex: 1, justifyContent: 'space-between', flexDirection: 'row', margin: 5}}
                >
                    {/* like */}
                    <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Icon 
                        type="material"
                        name="favorite-border"
                        size={30}
                    />
                    <Text style={{margin: 5}}>30K</Text>
                    </View>
                    {/* comment */}

                    <View style={{flexDirection: 'row', marginBottom: 5}}>
                    <Icon 
                        type="material"
                        name="comment-bank"
                        size={30}
                        color='gray'
                    />
                    <Text style={{margin: 5}}>30K</Text>
                    </View>

                    <Text
                        style={{ marginLeft: 10, marginBottom: 10, color: 'gray'}}
                    > 100K Shares</Text>

                    
                </View>

                

                <Divider />

            </View>



        </View>
    );
}
export default class ContentCard extends Component {
    render() {
        return (
            <View>


                <FlatList
                    data={DATA}
                    renderItem={({ item }) => <Content name={item.name} avatar={item.avatar} hour={item.time} image={item.image} captions={item.caption} />}

                />


            </View>
        )
    }
}



const styles = StyleSheet.create({
    content: {
        marginTop: 10,
        marginLeft: 10,
        flexDirection: 'row',
    },
    contentData: {
        marginTop: 10,
        marginBottom: 10
    },
    more: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 10,
    },
    profile: {
        flexDirection: 'column',
        marginLeft: 10
    },
    captions: {
        marginLeft: 10,
        marginBottom: 10
    }
})

