import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, Dimensions} from 'react-native';
import Headers from './Headers'
import MCard from './MCard'
// import {SearchScreen} from './Searchs'
import { Icon } from 'react-native-elements'
import ContentCard from './ContentCard';
// import {SearchBar} from 'native-base'
const {width: WIDTH} = Dimensions.get('screen')
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
      <ScrollView>
          <View style={styles.mainHead}>
            <Text style={styles.head}>Facebook</Text>
            <View style={styles.iconContainer}>
            <Icon
              name='favorite'
              type='material'
              color='#5389a6'
              onPress={() => console.log('hello')} />
              <Icon
              name='search'
              type='material'
              color='#5389a6'
              // onPress={() => {
              //   this.navigation.navigate('save')
              // }}
              />
             
            
            </View>
          </View>
          <Headers />
          <View style={styles.subHead}>
            <View style={styles.item}>
              <Icon
                name='videocam'
                type='material'
                color='#5389a6'
                onPress={() => console.log('hello')} />
                <Text style={styles.text}>Live</Text>
            </View>
            <View style={styles.item}>
              <Icon
                name='photo'
                type='material'
                color='#5389a6'
               
                 />
                <Text style={styles.text}>Photo</Text>
            </View>
            <View style={styles.item}>
            <Icon
                name='room'
                type='material'
                color='#5389a6' />
                <Text style={styles.text}>Check In</Text>
            </View>
          </View>
        <MCard />
        <ContentCard />
        </ScrollView>
      </SafeAreaView>
     
    );
  }
}

const styles = StyleSheet.create({
  container:{
      flex: 1,
      backgroundColor: 'whitesmoke'
      
  },
  head: {
    color: 'darkblue',
    fontSize: 30,
    fontStyle: 'normal',
    fontWeight: 'bold',
    marginLeft: 10
  },
  text: {
    padding: 5
  },
  mainHead: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  iconContainer: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginRight: 10
  },
  item:{
    flexDirection: 'row',
    backgroundColor: 'whitesmoke',
    paddingLeft: 10,
    paddingRight: 30,
    paddingBottom: 6,
    paddingTop: 6,
    marginRight: 3,
    marginLeft: 3,
    width: WIDTH / 3,
  },
  subHead: {
    flexDirection: 'row',
  }

})
