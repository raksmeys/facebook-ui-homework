import React, { Component } from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import { Avatar } from 'react-native-elements';
import MCard from './MCard'

export default class Headers extends Component {
    render() {
        return (
            <View style={styles.container}>

                <Avatar
                    rounded
                    source={{uri: 'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/94132053_2615136765441131_3936020631211999232_n.jpg?_nc_cat=102&ccb=2&_nc_sid=09cbfe&_nc_eui2=AeHt_JQShZMmlDvrRdLi3PzB2OcCnZfspMDY5wKdl-ykwLpDOpPvH9gsttRUPDGUI69QIBFcr6nfXBedGPOOLkbb&_nc_ohc=J8r-BgcNigcAX-r3GTo&_nc_ht=scontent.fpnh7-1.fna&oh=81ecab0f68cb6b97c076afc6f7769612&oe=5FDCB9D1'}}
                    />
                    <Text style={styles.color}>What's on your mind?</Text>
                   
                    
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 10
    },
    color: {
        color: 'darkblue',
        marginTop: 6,
        marginLeft: 10,
        fontSize: 17
    }
  })
